import { request } from '../../../utils/http-request'
import { setToken } from '../../../utils/token'

export const onSubmit = ({ email, password }, callback, history) => {
  const response = request({
    url: 'http://localhost:8181/auth',
    method: 'POST',
    data: {
      email, password
    }
  })
  response.then(res => {
    setToken(res.data.data.token)
    history.push('/veiculos')
  })
  .catch(res => {
    const{ data, status } = res.response
    if (status !== 200) {
      callback({
        error: true,
        message: data?.error?.message || 'Erro nao identificado, entre em contato com ADM'
      })
    }
  })
}

export default onSubmit
