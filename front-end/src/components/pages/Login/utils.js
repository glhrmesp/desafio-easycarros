import { object, string } from 'yup'

export const loginSchema = object().shape({
  email: string()
    .email('E-mail invalido')
    .required('Voce precisa informar um e-mail'),
  password: string()
    .required('Voce precisa informar uma senha')
})

export default loginSchema
