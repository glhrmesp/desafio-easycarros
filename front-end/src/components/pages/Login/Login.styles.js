import styled from 'styled-components'
import { Paper, Container } from '@material-ui/core'

export const LoginPaper = styled(Paper)`
  background: #fff;
  margin: auto;
  border-radius: 4px;
  padding: 30px;
  text-align: center;
`

export const LoginContainer = styled(Container)`
  height: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;

`

export const LoginForm = styled.form`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  > div {
    margin: 15px;
  }
`
