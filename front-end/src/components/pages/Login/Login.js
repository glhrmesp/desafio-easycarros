import React from 'react'
import {
  TextField,
  Button
} from '@material-ui/core'
import Alert from '@material-ui/lab/Alert'
import { useForm } from 'react-hook-form'
import { LoginForm, LoginPaper as Paper, LoginContainer as Container } from './Login.styles'
import { loginSchema } from './utils'
import { onSubmit as serviceOnSubmit } from './Login.services'


const Login = ({ history }) => {
  const {
    handleSubmit,
    errors,
    register
  } = useForm({ validationSchema: loginSchema })

  const [responseErrors, setResponseErrors] = React.useState({
    error: false,
    message: ''
  })

  const onSubmit = data => serviceOnSubmit(data, setResponseErrors, history)

  return (
    <Container>
      <Paper>
        <h1>Login</h1>
        <LoginForm
          onSubmit={handleSubmit(onSubmit)}
          autoComplete="off"
        >
          <TextField
            defaultValue="frontend-dev@easycarros.com"
            id="email"
            label="E-mail"
            type="email"
            name="email"
            variant="outlined"
            fullWidth
            inputRef={register}
            error={errors?.email ? !false : false}
            helperText={errors?.email?.message || ''}
          />
          <TextField
            defaultValue="Fr0nt3ndR0ck5!"
            id="password"
            label="Senha"
            variant="outlined"
            type="password"
            name="password"
            fullWidth
            inputRef={register}
            error={errors?.password ? !false : false}
            helperText={errors?.password?.message || ''}
          />
          <Button variant="contained" color="primary" type="submit">Entrar</Button>
          {
            responseErrors.error ? <Alert severity="error">{responseErrors.message}</Alert>
              : <></>
          }
        </LoginForm>
      </Paper>
    </Container>
  )
}

export default Login
