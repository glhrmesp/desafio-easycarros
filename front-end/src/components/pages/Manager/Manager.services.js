import { request } from '../../../utils/http-request'
import { getTokenBearer } from '../../../utils/token'

const url = 'http://localhost:8181/vehicle'

export const getCars = () => {
  return request({
    url,
    method: 'GET'
  },{
    Authorization: getTokenBearer()
  })
}

export const addCar = (plate, cars) => {
  return request({
    url,
    method: 'POST',
    data: {
      plate
    }
  }, {
    Authorization: getTokenBearer()
  }).then(res => {
    let listCars = cars.cars
    listCars.push(res.data.data)
    cars.setCars(listCars)
    alert('Adicionado com sucesso')
  })
}

export default getCars
