import { object, string } from 'yup'

export const plateSchema = object().shape({
  plate: string()
    .matches(/^[a-zA-Z0-9]{7}$/, {
      message: 'Formato invalido, insira algo parecido com XXX9999 ou XXX9X99',
    })
    .required()
})

export default plateSchema
