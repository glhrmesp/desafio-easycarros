import React, { useEffect, useState } from 'react'
import {
  Container,
  TextField,
  Paper,
  Icon,
  Button
} from '@material-ui/core'
import { Add } from '@material-ui/icons'
import { useForm } from 'react-hook-form'
import Table from '../../Table'
import { getCars, addCar } from './Manager.services'
import { plateSchema } from './utils'
import './Manager.styles.css'

const Manager = () => {
  const { register, errors, handleSubmit } = useForm({
    validationSchema:plateSchema
  })
  const [cars, setCars] = useState([])
  
  useEffect(() => {
    const mGetCars = async () => {
      const response = await getCars()
      setCars(response?.data?.data || [])
    }

    mGetCars()
  },[])

  const submit = ({ plate }) => addCar(plate, {cars, setCars})

  return (
    <Container className="rootContainer">
      <Paper>
        <form
          onSubmit={handleSubmit(submit)}
        >
          <TextField
            name="plate"
            label="Placa"
            inputRef={register}
            error={errors?.plate ? !false : false}
            helperText={errors?.plate ? errors.plate.message : ''}
          />
          <Button type="submit"><Add/></Button>
        </form>
      </Paper>
      <Table rows={cars} callback={setCars}/>
    </Container>
  )
}

export default Manager
