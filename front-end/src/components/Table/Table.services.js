import { request } from '../../utils/http-request'
import { getTokenBearer } from '../../utils/token'

export const handleDelete = (id, carList, cb) => {
  request({
    url: `http://localhost:8181/vehicle/${id}`,
    method: 'DELETE',
  },{
    Authorization: getTokenBearer()
  }).then(() => {
    const filtered = carList.filter(car => {
      if(car.id === id) return false
      return true
    })
    cb(filtered)
  }).catch(err => alert(err))
}