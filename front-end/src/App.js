import React from 'react'
import {
  MuiThemeProvider,
  createMuiTheme,
  CssBaseline
} from '@material-ui/core'
import { Router } from 'react-router-dom'
import { createBrowserHistory } from 'history'
import Routes from './routes'
import './App.css'

const theme = createMuiTheme({
  spacing: 0,
  drawerWidth: 240,
  typography: {
    useNextVariants: true
  }
})

const App = () => (
  <MuiThemeProvider theme={theme}>
    <CssBaseline />
    <Router history={createBrowserHistory({ basename: '/' })}>
      <Routes />
    </Router>
  </MuiThemeProvider>
)

export default App
