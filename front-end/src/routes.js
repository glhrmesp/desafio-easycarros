import { Route, Switch } from 'react-router-dom'
import React from 'react'
import Login from './components/pages/Login'
import Veiculos from './components/pages/Manager'

export default () => (
  <Switch>
    <Route exact path="/" component={Login} />
    <Route path="/veiculos" component={Veiculos} />
  </Switch>
)
