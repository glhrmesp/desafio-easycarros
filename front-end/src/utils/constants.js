import pkg from '../../package.json' //eslint-disable-line

export const { version } = pkg
export default { version }

// messages
export const REQUEST_GENERIC_ERROR = `
  Ops! Houve um problema com ]
  sua requisição. Tente novamente mais tarde!
`
export const REQUEST_FORBIDDEN_ERROR = 'Você não tem permissão'
export const REQUEST_UNAUTHORIZED_ERROR = 'Você esta deslogado!'

// events
export const REQUEST_ERROR_EVENT = 'requestErrorEvent'

// regex
// eslint-disable-next-line max-len
export const REGEX_EMAIL = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
export const REGEX_DATE = [/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/]
export const REGEX_ALPHANUMERIC = /[^a-z0-9 ]/gi
export const REGEX_NUMERIC = /[^0-9]/g
export const REGEX_NUMERIC_COMMA = /[^0-9,]/g
