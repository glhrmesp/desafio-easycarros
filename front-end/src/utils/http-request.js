import uuid from 'uuid/dist/v4'
import axios from 'axios'
import {
  REQUEST_ERROR_EVENT, REQUEST_GENERIC_ERROR, REQUEST_FORBIDDEN_ERROR, REQUEST_UNAUTHORIZED_ERROR
} from './constants'

const HTTP_STATUS = {
  UNAUTHORIZED: 401,
  FORBIDDEN: 403
}

const getErrorMessage = ({ data, status }) => {
  if (status && status === HTTP_STATUS.FORBIDDEN) {
    return REQUEST_FORBIDDEN_ERROR
  }
  if (data && Array.isArray(data.messages) && data.messages.length > 0) {
    return data.messages[0]
  }
  if (data && Array.isArray(data) && data.length > 0) {
    return data[0].message
  }
  if (data && data.messages) {
    return Object.values(data.messages || [])
  }
  if (data && Array.isArray(data.errors)) {
    return data.errors[0]
  }
  return REQUEST_GENERIC_ERROR
}

const handlerUnauthorizedError = () => {
  localStorage.clear()
  if (process.env.NODE_ENV && process.env.NODE_ENV === 'production') {
    window.location.href = '/'
    return
  }
  window.dispatchEvent(new CustomEvent(REQUEST_ERROR_EVENT, { detail: REQUEST_UNAUTHORIZED_ERROR }))
}

const handlerError = ({ response }) => {
  if (!response) return
  const { status } = response
  if (status && status === HTTP_STATUS.UNAUTHORIZED) {
    handlerUnauthorizedError()
    return
  }
  const message = response ? getErrorMessage(response) : REQUEST_GENERIC_ERROR
  window.dispatchEvent(new CustomEvent(REQUEST_ERROR_EVENT, { detail: message }))
}

export const request = async ({
  url,
  ...options
}, headers = {}) => axios({
  headers: {
    'Content-Type': 'application/json',
    ...headers
  },
  ...options,
  url
})

const configInterceptorResponse = () => {
  axios.interceptors.response.use(
    response => response,
    error => {
      handlerError(error)
      return Promise.reject(error)
    }
  )
}

const configInterceptorRequest = () => {
  axios.interceptors.request.use(config => {
    const cfg = config
    cfg.headers['Request-Id'] = uuid('request-id', uuid.DNS)
    return config
  })
  return axios
}

export const requestConfig = () => {
  configInterceptorRequest()
  configInterceptorResponse()
}
