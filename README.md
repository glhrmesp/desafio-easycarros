# Frontend Challenge

Desafio Easy Carros para frontend developers.

## FRONT-END

### Pré-requisitos

Você deve instalar os seguintes pacotes antes de prosseguir:

 - [`yarn`](https://yarnpkg.com/lang/en/docs/install)

### Executando o projeto

Na raiz deste projeto, rode:

```bash
cd ./frontend
yarn # irá instalar as dependencias
yarn start # o servidor irá executar o projeto react na porta 3000
```

## A API

### Pré-requisitos

Você deve instalar os seguintes pacotes antes de prosseguir:

 - [`yarn`](https://yarnpkg.com/lang/en/docs/install)

### Executando o servidor da API

Na raiz deste projeto, rode:

```bash
cd ./api
yarn install # vai demorar um pouco
yarn start # o servidor irá escutar a porta 8181 por padrão
```

Se tudo correr bem, você verá a seguinte mensagem no seu terminal:

> App is listening on http://localhost:8181

O CORS da API está configurado para receber requisições de `localhost:3000`

#### Solução de problemas

- Se ao executar `yarn start` você se deparar com um erro parecido com:

```
> Error: listen EADDRINUSE :::8181
>    at Server.setupListenHandle [as _listen2] (net.js:1330:14)
>    at listenInCluster (net.js:1378:12)
>    at Server.listen (net.js:1466:7)
>    ...
```

Significa que a porta `8181` está em uso no seu computador. Para utilizar uma porta diferente, rode:

```bash
PORT='<OUTRA PORTA>' yarn start
```


- Se tiver problemas com CORS:

Altere o arquivo .env para as configurações do app frontend
```
APP_HOST=localhost
APP_PORT=3000
```
